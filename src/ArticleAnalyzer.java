import java.util.*;

public class ArticleAnalyzer {

    private static int countOfStopWordsInArticle;

    private static List<String> getAllWordsFromArticle(String article) {

        return Arrays.asList(clearSymbolsAndMakeLowerCase(article).split("\\s+"));

    }


    static ArticleAnalyseInfo analyseArticle(String article, Set<String> stopWords) {
        List<String> allWords = getAllWordsFromArticle(article);
        Map<String, Integer> wordsOccurrences = new HashMap<>();

        for (String word : allWords) {
            if (stopWords.contains(word)) {
                countOfStopWordsInArticle++;
            } else {
                int i = wordsOccurrences.get(word) == null ? 0 : wordsOccurrences.get(word);

                wordsOccurrences.put(word, i + 1);
            }
        }

        ArticleAnalyseInfo articleAnalyseInfo = new ArticleAnalyseInfo();

        for (String word : wordsOccurrences.keySet()) {
            int wordCount = wordsOccurrences.get(word);
            articleAnalyseInfo.getSharePerCorpus().put(word, ((double) wordCount / allWords.size()));
            articleAnalyseInfo.getSharePerPureCorpus().put(word, ((double) wordCount / (allWords.size() - countOfStopWordsInArticle)));
        }
        return articleAnalyseInfo;

    }


    private static String clearSymbolsAndMakeLowerCase(String string) {

        string = string.replaceAll("[^a-zA-Z-']", " ").toLowerCase();
        return string;

    }

}
