import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {

        String article = "The Moon is in synchronous rotation with Earth, and thus always shows the same " +
                "side to Earth, the near side. Because of libration, slightly more than half (about 59%) of the" +
                " total lunar surface can be viewed from Earth.[15] The near side is marked by dark volcanic maria that " +
                "fill the spaces between the bright ancient crustal highlands and the prominent impact craters. After the Sun, the " +
                "Moon is the second-brightest celestial object regularly visible in Earth's sky. ";

        String arr[] = {"is", "in", "the", "and", "can", "be", "out"};
        Set<String> stopWords = new HashSet<>(Arrays.asList(arr));

        ArticleAnalyseInfo articleAnalyseInfo =  ArticleAnalyzer.analyseArticle(article, stopWords);
        for (String s : articleAnalyseInfo.getSharePerCorpus().keySet()) {
            System.out.println(s + " ---- sharePerCorpus is: " + articleAnalyseInfo.getSharePerCorpus().get(s) + " ---- sharePerPureCorpus is: " + articleAnalyseInfo.getSharePerPureCorpus().get(s));
        }

    }
}
