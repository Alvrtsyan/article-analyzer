import java.util.HashMap;
import java.util.Map;

public  class ArticleAnalyseInfo {

    private Map<String, Double> sharePerCorpus = new HashMap<>();
    private Map<String, Double> sharePerPureCorpus = new HashMap<>();



    public Map<String, Double> getSharePerCorpus() {
        return sharePerCorpus;
    }

    public void setSharePerCorpus(Map<String, Double> sharePerCorpus) {
        this.sharePerCorpus = sharePerCorpus;
    }

    public Map<String, Double> getSharePerPureCorpus() {
        return sharePerPureCorpus;
    }

    public void setSharePerPureCorpus(Map<String, Double> sharePerPureCorpus) {
        this.sharePerPureCorpus = sharePerPureCorpus;
    }
}
